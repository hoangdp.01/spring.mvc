DROP TABLE IF EXISTS allergic_foods;
CREATE TABLE allergic_foods
(
    id         CHAR(36)     NOT NULL,
    name       VARCHAR(255) NOT NULL,
    is_active  BIT(1)       NOT NULL DEFAULT 1,
    created_at DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME              DEFAULT NULL,
    PRIMARY KEY (id)
) CHARACTER SET = UTF8MB4
  COLLATE = UTF8MB4_GENERAL_CI;

# INSERT INTO allergic_foods (id, name)
# VALUES ('9eb57e76-c257-4314-bd2f-43906af110eb', 'Coca-cola'),
#        ('954ef39b-8bd6-4171-87ac-5701ed8066g4', 'Mật ong'),
#        ('19e1d6a3-91c8-4787-9f80-6e021ba3b0n2', 'Nước chanh'),
#        ('de1fa92d-6719-4523-9040-2f2f04ff6am2', 'Gừng');
