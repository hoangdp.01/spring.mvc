DROP TABLE IF EXISTS product_allergen;
CREATE TABLE product_allergic_food_mappings
(
    id          CHAR(36) NOT NULL,
    product_id  CHAR(36) NOT NULL,
    allergic_food_id CHAR(36) NOT NULL,
    is_active   BIT(1)   NOT NULL DEFAULT 1 COMMENT '0: in-active, 1: active',
    created_at  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at  DATETIME          DEFAULT NULL
) CHARACTER
      SET
      = utf8mb4
  COLLATE = utf8mb4_general_ci;