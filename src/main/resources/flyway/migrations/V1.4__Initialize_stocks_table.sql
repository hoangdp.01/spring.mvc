DROP TABLE IF EXISTS stocks;
CREATE TABLE stocks
(
    id         CHAR(36)     NOT NULL,
    name       VARCHAR(255) NOT NULL,
    is_active  BIT(1)       NOT NULL DEFAULT 1 COMMENT '0: in-active, 1: active',
    created_at DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME              DEFAULT NULL
) CHARACTER
      SET
      = utf8mb4
  COLLATE = utf8mb4_general_ci;

# INSERT INTO stocks (id, name)
# VALUES ('3353e9e0-7a99-4f94-bbca-cfca87442332', 'Gạo tám thơm'),
#        ('954ef39b-8bd6-4171-87ac-5701ed806695', 'Thịt gà'),
#        ('19e1d6a3-91c8-4787-9f80-6e021ba3b012', 'Mắm tôm'),
#        ('19e1d6a3-91c8-4787-9f80-6e021ba3b012', 'Bánh tráng'),
#        ('19e1d6a3-91c8-4787-9f80-6e021ba3b012', 'Nước mắm'),
#        ('19e1d6a3-91c8-4787-9f80-6e021ba3b012', 'Tương ớt'),
#        ('de1fa92d-6719-4523-9040-2f2f04ff6a7f', 'Thịt bò');