TRUNCATE TABLE products;

ALTER TABLE products
    MODIFY COLUMN `id` CHAR(36) NOT NULL,
    MODIFY COLUMN `image_path` VARCHAR(255),
    ADD COLUMN `image_name` VARCHAR(100) AFTER `image_path`,
    ADD COLUMN `start_date_sale` DATETIME NOT NULL AFTER `description`,
    ADD COLUMN `end_date_sale` DATETIME NOT NULL AFTER `start_date_sale`,
    ADD COLUMN `product_type` VARCHAR(100) NOT NULL AFTER `end_date_sale`,
    ADD COLUMN `discount` INT AFTER `product_type`,
    ADD COLUMN `start_date_discount` DATETIME NOT NULL AFTER `discount`,
    ADD COLUMN `end_date_discount` DATETIME NOT NULL AFTER `start_date_discount`,
    DROP quantity;

# INSERT INTO products
# (`id`,
#  `name`,
#  `image_path`,
#  `image_name`,
#  `price`,
#  `description`,
#  `start_date_sale`,
#  `end_date_sale`,
#  `product_type`,
#  `discount`,
#  `start_date_discount`,
#  `end_date_discount`,
#  `is_active`,
#  `created_at`,
#  `updated_at`)
# VALUES /*Value 01*/
# ( '5a2defe5-ad54-497b-859a-a7f056228275'
# , 'Cơm gà nấm'
# , NULL
# , 'image_name_01'
# , 5000000
# , 'description of production 01'
# , CURRENT_TIMESTAMP
# , CURRENT_TIMESTAMP
# , 'FOOD'
# , 10
# , CURRENT_TIMESTAMP
# , CURRENT_TIMESTAMP
# , 1
# , CURRENT_TIMESTAMP
# , NULL),
#     /*Value 02*/
# ( '4a7bc060-9cfd-467e-ba7a-8be5b273fb5a'
# , 'Xôi gà cay'
# , NULL
# , 'image_name_01'
# , 5000000
# , 'description of production 02'
# , CURRENT_TIMESTAMP
# , CURRENT_TIMESTAMP
# , 'WINE'
# , 20
# , CURRENT_TIMESTAMP
# , CURRENT_TIMESTAMP
# , 1
# , CURRENT_TIMESTAMP
# , NULL);

