DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts
(
    id         CHAR(36)     NOT NULL,
    email      VARCHAR(255) NOT NULL UNIQUE,
    password   VARCHAR(100) NOT NULL,
    name       VARCHAR(255) NOT NULL,
    birthday   DATE         NOT NULL,
    gender     CHAR(1)      NOT NULL comment 'M: Male, F: Female',
    role       CHAR(1)      NOT NULL comment 'U: User, A: Admin',
    is_active  BIT(1)       NOT NULL DEFAULT 1 comment '0: in-active, 1: active',
    created_at DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME              DEFAULT NULL,
    PRIMARY key (id)
) CHARACTER
      SET
      = utf8mb4
  COLLATE = utf8mb4_general_ci;

# data init
# admin123: EtA7fscotMV7d/wClUWYzA==
# 123456: w2oYoBlcIvmh9VYIHhEO8w==
# INSERT INTO accounts(id, email, password, name, birthday, gender, role)
# VALUES ('fae57a43-ed66-4f83-a683-5f8e58393fbf', 'admin@gmail.com', 'EtA7fscotMV7d/wClUWYzA==', 'hoang_dp_01',
#         str_to_date('1993,03,23', '%Y,%m,%d'), 'M', 'A')
#         ,
#        ('8a573e6d-cc9b-43d3-babb-9ec924b9acae', 'default@gmail.com', 'w2oYoBlcIvmh9VYIHhEO8w==', 'hoang_dp_02',
#         str_to_date('1994,04,24', '%Y,%m,%d'), 'M', 'U')
#         ,
#        ('6e008195-afe1-44c0-8d64-725003277773', 'test@gmail.com', 'w2oYoBlcIvmh9VYIHhEO8w==', 'hoang_dp_03',
#         str_to_date('1995,05,25', '%Y,%m,%d'), 'F', 'U')
#         ,
#        ('c7a109c6-5343-4ce0-8e82-76269935cfdf', 'hoangdp@gmail.com', 'w2oYoBlcIvmh9VYIHhEO8w==', 'hoang_dp_04',
#         str_to_date('1996,06,26', '%Y,%m,%d'), 'M', 'U');