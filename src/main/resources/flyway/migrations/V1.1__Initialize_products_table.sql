# Create table products
DROP TABLE IF EXISTS products;
CREATE TABLE products
(
    id          	INT(5)       NOT NULL AUTO_INCREMENT,
    name        	VARCHAR(255) NOT NULL,
    image_path  	VARCHAR(255),
    price       	DOUBLE       NOT NULL,
    quantity    	INT          NOT NULL,
    description 	TEXT,
    is_active   	BIT(1)       NOT NULL DEFAULT 1,
    created_at  	DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at  	DATETIME              DEFAULT NULL,
    PRIMARY KEY (id)
) CHARACTER SET = UTF8MB4
  COLLATE = UTF8MB4_GENERAL_CI;

# Init data
# INSERT INTO products(name, price, quantity)
# VALUES ('Cơm gà nấm', 150000, 100),
#        ('Xôi gà cay', 250000, 200),
#        ('Lẩu mắm ninh kiều', 350000, 300),
#        ('Bún đậu mắm tôm', 450000, 400),
#        ('Phở bò tái gầu', 550000, 500),
#        ('Bánh tráng trộn', 650000, 600);