package com.springmvc.application.controllers.login;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springmvc.infrastructure.dao.account.AccountDao;
import com.springmvc.infrastructure.dao.account.AccountRecord;

@Controller
public class LoginController {

	@Autowired
	private AccountDao accountDao;

	@GetMapping({ "/", "/login" })
	public String hello(@RequestParam(value = "name", required = false, defaultValue = "World") String name,
			Model model) {
		
		Optional<AccountRecord> aaaaa = accountDao.getById(22);
		
		System.out.println(aaaaa.get().getEmail());
		model.addAttribute("name", name);
		return "login/login";
	}
}
