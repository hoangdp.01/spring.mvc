package com.springmvc.domain.repositories.account;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.springmvc.domain.model.account.Account;
import com.springmvc.domain.repositories.IRepository;
import com.springmvc.infrastructure.dao.account.AccountRecord;

@Repository
public class AccountRepository implements IRepository<Account, AccountRecord> {

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public Account record2Entity(AccountRecord record) {
		return modelMapper.map(record, Account.class);
	}

	@Override
	public AccountRecord entity2Record(Account entity) {
		return modelMapper.map(entity, AccountRecord.class);
	}

}
