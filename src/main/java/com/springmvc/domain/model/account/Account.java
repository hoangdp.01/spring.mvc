package com.springmvc.domain.model.account;

import java.time.LocalDate;
import java.util.Optional;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account {
	private long id;
	private String email;
	private String password;
	private String name;
	private LocalDate birthday;
	private String gender;
	private String role;
	private String isActive;
	private String createdAt;
	private Optional<String> updatedAt;
}
