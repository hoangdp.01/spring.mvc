package com.springmvc.domain.model.account.vo;

public enum AccountRole {
	GUEST("G"), ADMIN("AD"), USER("U");

	public final String value;

	private AccountRole(String value) {
		this.value = value;
	}
}
