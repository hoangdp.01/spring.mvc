package com.springmvc.domain.model.account.vo;

public enum AccountGender {
	FEMALE("FM"), MALE("M");

	public final String value;

	private AccountGender(String value) {
		this.value = value;
	}
}