package com.springmvc.infrastructure.dao;

import java.util.List;
import java.util.Optional;

public interface IDao<T> {
	public Optional<T> getById(long id);

	public List<T> getAll();

	public int save(T t);

	public int update(T t, String[] params);

	public int delete(T t);
}
