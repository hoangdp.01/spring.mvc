package com.springmvc.infrastructure.dao.account;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.springmvc.infrastructure.dao.IDao;

public class AccountDao implements IDao<AccountRecord> {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	public Optional<AccountRecord> getById(long id) {
		Optional<AccountRecord> accountRecord = Optional.ofNullable(jdbcTemplate
				.queryForObject("select * from accounts where id = ?", new Object[] { id }, new AccountRecordMapper()));
		return accountRecord;
	}

	@Override
	
	public List<AccountRecord> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	
	public int save(AccountRecord t) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	
	public int update(AccountRecord t, String[] params) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	
	public int delete(AccountRecord t) {
		// TODO Auto-generated method stub
		return 0;
	}

}
