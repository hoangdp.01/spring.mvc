package com.springmvc.infrastructure.dao.account;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.jdbc.core.RowMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Table(name = "accounts", schema = "spring.mvc.dev")
public class AccountRecord {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "birthday", nullable = false)
	private Date birthday;

	@Column(name = "gender", nullable = false)
	private String gender;

	@Column(name = "role", nullable = false)
	private String role;

	@Column(name = "is_active", nullable = false)
	private Boolean isActive;

	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@Column(name = "updated_at", nullable = true)
	private Optional<Date> updatedAt;

}

class AccountRecordMapper implements RowMapper<AccountRecord> {
	public AccountRecord mapRow(ResultSet rs, int row) throws SQLException {
		AccountRecord accountRecord = new AccountRecord();
		accountRecord.setId(rs.getInt("id"));
		accountRecord.setEmail(rs.getString("email"));
		accountRecord.setPassword(rs.getString("password"));
		accountRecord.setBirthday(rs.getDate("birthday"));
		accountRecord.setGender(rs.getString("gender"));
		accountRecord.setRole(rs.getString("role"));
		accountRecord.setIsActive(rs.getBoolean("is_active"));
		accountRecord.setCreatedAt(rs.getDate("created_at"));
		accountRecord.setUpdatedAt(Optional.ofNullable(rs.getDate("updated_at")));
		return accountRecord;
	}
}
